//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acConnectExecute(TObject *Sender)
{
	IdTCPClient->Host = edHost->Text;
	IdTCPClient->Port = StrToInt(edPort->Text);
    IdTCPClient->Connect();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acDisconnectExecute(TObject *Sender)
{
   IdTCPClient->Disconnect();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acGetTimeExecute(TObject *Sender)
{
	IdTCPClient->Socket->WriteLn("time");
	UnicodeString x;
	x = IdTCPClient->Socket->ReadLn();
	me->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acGetStrExecute(TObject *Sender)
{
	IdTCPClient->Socket->WriteLn("str");
	UnicodeString x;
	x = IdTCPClient->Socket->ReadLn(IndyTextEncoding_UTF8());
	me->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acGetImageExecute(TObject *Sender)
{
		IdTCPClient->Socket->WriteLn("image");
		TMemoryStream *x = new TMemoryStream();
		try {
		int xSize = IdTCPClient->Socket->ReadInt64();
		IdTCPClient->Socket->ReadStream(x, xSize);
		im->Bitmap->LoadFromStream(x);
		}
		__finally
		{
        delete x;
		}

}
//---------------------------------------------------------------------------
void __fastcall TForm1::alUpdate(TBasicAction *Action, bool &Handled)
{
	edHost->Enabled = !IdTCPClient->Connected();
	edPort->Enabled = !IdTCPClient->Connected();
	acConnect->Enabled = !IdTCPClient->Connected();
	acDisconnect->Enabled = IdTCPClient->Connected();
	acGetTime->Enabled = IdTCPClient->Connected();
	acGetStr->Enabled = IdTCPClient->Connected();
	acGetImage->Enabled = IdTCPClient->Connected();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acFileExecute(TObject *Sender)
{
	IdTCPClient->Socket->WriteLn("file");
}
//---------------------------------------------------------------------------
