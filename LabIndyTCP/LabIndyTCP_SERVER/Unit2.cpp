//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm2::buStartClick(TObject *Sender)
{
	IdTCPServer->Active = true;
    me->Lines->Add("Active = true");
}
//---------------------------------------------------------------------------
void __fastcall TForm2::buStopClick(TObject *Sender)
{
	IdTCPServer->Active = false;
    me->Lines->Add("Active = false");
}
//---------------------------------------------------------------------------
void __fastcall TForm2::IdTCPServerConnect(TIdContext *AContext)
{
	me->Lines->Add(Format("[%s] - Client connected",
		ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))
	));
}
//---------------------------------------------------------------------------
void __fastcall TForm2::IdTCPServerDisconnect(TIdContext *AContext)
{
	me->Lines->Add(Format("[%s] - Client disconnected",
		ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))
	));
}
//---------------------------------------------------------------------------
void __fastcall TForm2::IdTCPServerExecute(TIdContext *AContext)
{
	UnicodeString x = AContext->Connection->Socket->ReadLn();
	me->Lines->Add("Input = " + x);
	//
	if (x == "time") {
	AContext->Connection->Socket->WriteLn(TimeToStr(Now()));
	}
	if (x == "str") {
	AContext->Connection->Socket->WriteLn(edStr->Text,
		IndyTextEncoding_UTF8());
	}
		if (x == "file") {
	AContext->Connection->Socket->WriteLn(Edit2->Text,
		IndyTextEncoding_UTF8());
	}
	if (x == "image") {
		TMemoryStream *x = new TMemoryStream();
		try {
		im->Bitmap->SaveToStream(x);
		x->Seek(0, 0);
		AContext->Connection->Socket->Write(x->Size);
		AContext->Connection->Socket->Write(x);
		}
		__finally {
		delete x;
		}
	}
}
//---------------------------------------------------------------------------
