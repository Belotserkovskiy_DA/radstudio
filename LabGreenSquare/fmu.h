//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiStart;
	TTabItem *tiPlay;
	TLayout *lyTop;
	TButton *Button1;
	TLabel *Label1;
	TLabel *laTime;
	TGridPanelLayout *GridPanelLayout1;
	TLayout *lyBot;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *lyCenter;
	TButton *bu1;
	TButton *bu2;
	TButton *bu3;
	TButton *bu4;
	TButton *bu5;
	TButton *bu6;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TButton *Button2;
	TLabel *Label3;
	TTimer *tmPlay;
	TTabItem *tiLose;
	TTabItem *tiWin;
	TLabel *Label2;
	TLabel *Label4;
	TImage *Image1;
	TButton *Button3;
	TImage *Image2;
	TButton *Button4;
	TImage *Image3;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall bu1Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
private:
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimeValue;
	TList *FListBox;
	TList *FListAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();
	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
	const int cMaxBox = 25;
	const int cMaxAnswer = 6;
	const int cMinPossible =4 ;
	const int cMaxPossible = 14;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
