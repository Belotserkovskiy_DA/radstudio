//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
	tc->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
		tc->ActiveTab = tiPlay;
		DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FListBox = new TList;
	for (int i = 1; i <= cMaxBox; i++) {
		FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}
	//
	FListAnswer = new TList;
	FListAnswer->Add(bu1);
	FListAnswer->Add(bu2);
	FListAnswer->Add(bu3);
	FListAnswer->Add(bu4);
	FListAnswer->Add(bu5);
	FListAnswer->Add(bu6);

}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (double)30/(24*60*60);
	tmPlay->Enabled = true;
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoContinue()
{
	for (int i = 0; i < cMaxBox; i++)
	{
		((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::White;
	}
	//
	FNumberCorrect = RandomRange(cMinPossible,cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	for (int i = 0; i < FNumberCorrect; i++)
	{
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Green;
	}
	//
	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if (xAnswerStart < cMinPossible)
		xAnswerStart = cMinPossible;
	//
	for (int i = 0; i < cMaxAnswer; i++)
	{
		((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart + i);
	}
}
//---------------------------------------------------------------------------
void Tfm::DoAnswer(int aValue)
{
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if (FCountWrong > 5)
	{ tc->ActiveTab = tiLose;}
	if (FCountCorrect > 5)
	{ tc->ActiveTab = tiWin;}
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoFinish()
{
	tmPlay->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if (x <= 0)
	{
		DoFinish();
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::bu1Click(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
	FTimeValue = Time().Val + (double)30/(24*60*60);

}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button4Click(TObject *Sender)
{
tc->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
tc->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------

