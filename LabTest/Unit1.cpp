//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Memo1->Lines->Clear();
	tc1->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button14Click(TObject *Sender)
{
	tc1->ActiveTab = ti1;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	Memo1->Lines->Add(
	L"Qestion " + tc1->ActiveTab->Text +" - " +
	((((TControl *)Sender)->Tag == 1) ? L"true" : L"false")
	);
	tc1->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	pb1->Max = tc1->TabCount-1;
	Button14Click(Button14);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::tc1Change(TObject *Sender)
{
	pb1->Value = tc1->ActiveTab->Index;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::infoClick(TObject *Sender)
{
ShowMessage("�������������� �������, ������ 174-332.");
}
//---------------------------------------------------------------------------
