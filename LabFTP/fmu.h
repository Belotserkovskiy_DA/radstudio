//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdFTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TGridPanelLayout *gpl;
	TButton *buConnect;
	TIdFTP *IdFTP;
	TListBox *lb;
	TTabControl *tc;
	TTabItem *tiImage;
	TTabItem *tiOther;
	TTabItem *tiText;
	TImage *im;
	TMemo *me;
	TLabel *Label1;
	TButton *Button1;
	void __fastcall buConnectClick(TObject *Sender);
	void __fastcall lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item);
	void __fastcall Button1Click(TObject *Sender);

private:	// User declarations
public:
UnicodeString ext;
UnicodeString nam;	// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
