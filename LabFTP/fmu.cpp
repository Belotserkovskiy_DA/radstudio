//---------------------------------------------------------------------------
#include <string>
#include <fstream>
#include <iostream>
#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
tc->ActiveTab = tiImage;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buConnectClick(TObject *Sender)
{
	IdFTP->Host = "46.188.34.242.";
	IdFTP->Username = "test";
	IdFTP->Password = "test";
	IdFTP->Passive = True;
	IdFTP->Connect();
	IdFTP->ChangeDir("/ftp/");
	//
	IdFTP->List(lb->Items,"",false);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	UnicodeString xExt = Ioutils::TPath::GetExtension(Item->Text);
	nam = Ioutils::TPath::GetFileName(Item->Text);
	TMemoryStream *x = new TMemoryStream();
	ext = Item->Text;
	try {
		if (xExt == ".txt") {
			tc->ActiveTab = tiText;
			IdFTP->Get(Item->Text, x, true);
			x->Seek(0,0);
			me->Lines->LoadFromStream(x);
		} else
		if (xExt == ".png" || xExt == ".jpg") {
			tc->ActiveTab = tiImage;
			IdFTP->Get(Item->Text, x, true);
			x->Seek(0,0);
			im->Bitmap->LoadFromStream(x);
		} else
		tc->ActiveTab = tiOther;
	}
	__finally {
	delete x;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	TMemoryStream *x = new TMemoryStream();
	IdFTP->Get(ext, x, true);
	x->SaveToFile(nam);

}
//---------------------------------------------------------------------------

