//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiplay;
	TTabItem *tihelp;
	TPanel *Panel1;
	TButton *butstart;
	TButton *butrules;
	TButton *butabout;
	TButton *butexit;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *Button1;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TButton *Button2;
	TLabel *Label3;
	TTabItem *TabItem4;
	TToolBar *ToolBar3;
	TLabel *Label4;
	TButton *Button3;
	TMemo *Memo1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buteasy;
	TButton *butnorm;
	TButton *buthard;
	TButton *butvhard;
	TButton *butimp;
	TStyleBook *StyleBook1;
	void __fastcall butstartClick(TObject *Sender);
	void __fastcall butrulesClick(TObject *Sender);
	void __fastcall butaboutClick(TObject *Sender);
	void __fastcall butexitClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
