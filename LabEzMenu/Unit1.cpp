//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::butstartClick(TObject *Sender)
{
	tc->ActiveTab = tiplay;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::butrulesClick(TObject *Sender)
{
	tc->ActiveTab = tihelp;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::butaboutClick(TObject *Sender)
{
 About->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::butexitClick(TObject *Sender)
{
 Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
if ((tc->ActiveTab != tiMain) && (Key == vkHardwareBack)) {
	tc->ActiveTab = tiMain;
	Key = 0;
}
}
//---------------------------------------------------------------------------

