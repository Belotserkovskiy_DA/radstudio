//---------------------------------------------------------------------------

#ifndef fmhuH
#define fmhuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfmh : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TButton *Button1;
	TImage *Image1;
	TStyleBook *StyleBook1;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfmh(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfmh *fmh;
//---------------------------------------------------------------------------
#endif
