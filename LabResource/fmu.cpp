//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "fmhu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void LoadResourceToImage(UnicodeString aResName, TBitmap* aResult)
{
	TResourceStream* x;
	x=new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try
	{
		aResult->LoadFromStream(x);
	}
	__finally {
		delete x;
	}
}
UnicodeString LoadResourceToText(UnicodeString aResName)
{
	TResourceStream* x;
	TStringStream* xSS;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try
	{
		xSS = new TStringStream("", TEncoding::ANSI, true);
		try
		{
			xSS->LoadFromStream(x);
			return xSS->DataString;
		}
		__finally
		{
			delete xSS;
        }
	}
	__finally
	{
		delete x;
    }
}
void __fastcall TForm1::Button1Click(TObject *Sender)
{
LoadResourceToImage("wyv1", im->Bitmap);
me->Lines->Text = LoadResourceToText("wyv2");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
LoadResourceToImage("cer1", im->Bitmap);
me->Lines->Text = LoadResourceToText("cer2");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
LoadResourceToImage("man1", im->Bitmap);
me->Lines->Text = LoadResourceToText("man2");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
LoadResourceToImage("hip1", im->Bitmap);
me->Lines->Text = LoadResourceToText("hip2");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
fmh->ShowModal();
}
//---------------------------------------------------------------------------
