//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc1->GotoVisibleTab(ti1->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::butinfoClick(TObject *Sender)
{
	tc1->GotoVisibleTab(ti3->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormShow(TObject *Sender)
{
dm->FDConnection1->Connected = true;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
        tc1->ActiveTab = ti1;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::butaddClick(TObject *Sender)
{
	dm->taNotes->Append();
	tc1->GotoVisibleTab(ti2->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->taNotes->Edit();
	tc1->GotoVisibleTab(ti2->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::butsaveClick(TObject *Sender)
{
	dm->taNotes->Post();
	tc1->GotoVisibleTab(ti1->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::butbackClick(TObject *Sender)
{
	dm->taNotes->Cancel();
	tc1->GotoVisibleTab(ti1->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::butdeleteClick(TObject *Sender)
{
	dm->taNotes->Delete();
	tc1->GotoVisibleTab(ti1->Index);
}
//---------------------------------------------------------------------------

