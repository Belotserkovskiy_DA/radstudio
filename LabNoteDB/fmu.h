//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Effects.hpp>
#include "dmu.h"
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TTabControl *tc1;
	TTabItem *ti1;
	TTabItem *ti2;
	TLayout *Layout2;
	TButton *butadd;
	TLayout *Layout3;
	TGridPanelLayout *GridPanelLayout1;
	TButton *butsave;
	TButton *butback;
	TButton *butdelete;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label2;
	TEdit *edit;
	TLabel *Label3;
	TTrackBar *prior;
	TLabel *Label4;
	TMemo *memo;
	TStyleBook *StyleBook1;
	TButton *butinfo;
	TTabItem *ti3;
	TLabel *Label5;
	TImage *Image1;
	TLabel *Label6;
	TButton *Button1;
	TInnerGlowEffect *InnerGlowEffect1;
	TInnerGlowEffect *InnerGlowEffect2;
	TInnerGlowEffect *InnerGlowEffect3;
	TInnerGlowEffect *InnerGlowEffect4;
	TInnerGlowEffect *InnerGlowEffect5;
	TInnerGlowEffect *InnerGlowEffect6;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkControlToField *LinkControlToField3;
	TListView *ListView1;
	TLinkListControlToField *LinkListControlToField1;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall butinfoClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall butaddClick(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall butsaveClick(TObject *Sender);
	void __fastcall butbackClick(TObject *Sender);
	void __fastcall butdeleteClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
