#ifndef ClientClassesUnit1H
#define ClientClassesUnit1H

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TdmDatabaseClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
  public:
    __fastcall TdmDatabaseClient(TDBXConnection *ADBXConnection);
    __fastcall TdmDatabaseClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmDatabaseClient();
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
  };

#endif
