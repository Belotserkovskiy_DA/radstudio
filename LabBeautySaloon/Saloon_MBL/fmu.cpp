//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
tc->ActiveTab = ti1;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
 tc->GotoVisibleTab(ti2->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
 tc->GotoVisibleTab(ti1->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
 tc->GotoVisibleTab(ti2->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView2ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
 tc->GotoVisibleTab(ti3->Index);
}
//---------------------------------------------------------------------------

