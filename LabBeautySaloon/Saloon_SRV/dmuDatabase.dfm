object dmDatabase: TdmDatabase
  OldCreateOrder = False
  Height = 345
  Width = 432
  object BeatysaloonConnection: TSQLConnection
    ConnectionName = 'BeatySaloon'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      
        'Database=C:\Users\VD\Documents\radstudio\LabBeautySaloon\BEAUTYS' +
        'ALOON.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    Connected = True
    Left = 97
    Top = 84
  end
  object taCategory: TSQLDataSet
    CommandText = 'CATEGORY'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = BeatysaloonConnection
    Left = 97
    Top = 132
    object taCategoryID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 200
    end
  end
  object taService: TSQLDataSet
    CommandText = 'SERVICE'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = BeatysaloonConnection
    Left = 283
    Top = 100
    object taServiceID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taServiceCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Required = True
    end
    object taServiceNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object taServicePRICE: TSingleField
      FieldName = 'PRICE'
    end
    object taServiceDURATION: TIntegerField
      FieldName = 'DURATION'
    end
  end
  object dspService: TDataSetProvider
    DataSet = taService
    Left = 288
    Top = 224
  end
  object dspCategory: TDataSetProvider
    DataSet = taCategory
    Left = 176
    Top = 248
  end
end
