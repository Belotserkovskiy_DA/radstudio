//---------------------------------------------------------------------------

#ifndef dmuDatabaseH
#define dmuDatabaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmDatabase : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *BeatysaloonConnection;
	TSQLDataSet *taCategory;
	TSQLDataSet *taService;
	TDataSetProvider *dspService;
	TDataSetProvider *dspCategory;
	TIntegerField *taCategoryID;
	TWideStringField *taCategoryNAME;
	TIntegerField *taServiceID;
	TIntegerField *taServiceCATEGORY_ID;
	TWideStringField *taServiceNAME;
	TSingleField *taServicePRICE;
	TIntegerField *taServiceDURATION;

private:	// User declarations
public:		// User declarations
	__fastcall TdmDatabase(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmDatabase *dmDatabase;
//---------------------------------------------------------------------------
#endif

