//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
	Timer1->Enabled = false;
	Time().Val = 0;
	FTimeValue = Time().Val;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::restartClick(TObject *Sender)
{
	FTimeValue = Time().Val;
	FTimePause = false;
	Timer1->Enabled = true;
	Label2->Text = " ";
	Timer1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::pauseClick(TObject *Sender)
{
	if (FTimePause) {
		FTimeValue = Time().Val + FTimeValue;
		FTimePause = false;
		Timer1->Enabled = true;
		pause->Text = L"�����";
		restart->Enabled = true;
		reduce->Enabled = true;
	} else {
		Timer1->Enabled = false;
		FTimePause = true;
		FTimeValue = FTimeValue - Time().Val;
		pause->Text = L"������";
		restart->Enabled = false;
		reduce->Enabled = false;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::reduceClick(TObject *Sender)
{
	//FTimeValue = FTimeValue - (double)10/(24*60*60);
	Label2->Text = Label1->Text + " " + Label2->Text;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Timer1Timer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
 //	if (x <= 0) {
 //		x = 0;
  //		Timer1->Enabled = false;
  //	}
	Label1->Text = FormatDateTime("nn:ss", x);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormResize(TObject *Sender)
{
	if (ClientHeight<500) {
		ClientHeight=500;
	}
	if (ClientWidth<500) {
		ClientWidth=500;
	}
}
//---------------------------------------------------------------------------
