//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	Label2->Position->X = - Label2->Width;
	Label3->Position->X = Label3->Width;
	Label4->Position->X = - Label4->Width;
	TAnimator::AnimateIntWait(Label2,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Label3,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Label4,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	Label1->AutoSize = true;
	Label1->TextSettings->Font->Size = 400;
	TAnimator::AnimateIntWait(Label1, "TextSettings.Font.Size",32,1,TAnimationType::Out, TInterpolationType::Linear);
	Label1->AutoSize = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
	int y = GridPanelLayout1->Position->Y;
	GridPanelLayout1->Position->Y= - GridPanelLayout1->Height;
	TAnimator::AnimateIntWait(GridPanelLayout1,"Position.Y",y,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button7Click(TObject *Sender)
{
	Image1->Position->X = -  Image1->Width;
	Image2->Position->X =  Image2->Width;
	TAnimator::AnimateIntWait(Image1,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Image2,"Position.X",0,320,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
