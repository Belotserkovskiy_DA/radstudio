//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <System.Rtti.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <System.Bindings.Outputs.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TBindingsList *BindingsList1;
	TBindSourceDB *dbOrdering;
	TBindSourceDB *dbProduct;
	TTabControl *tc;
	TTabItem *timenu;
	TTabItem *tipizzas;
	TTabItem *tipizza;
	TTabItem *tiress;
	TTabItem *tires;
	TTabItem *tifeed;
	TTabItem *tireport;
	TTabItem *tidelivery;
	TTabItem *ticontact;
	TGridPanelLayout *GridPanelLayout1;
	TGridPanelLayout *GridPanelLayout3;
	TImage *Image1;
	TButton *bupizzas;
	TLabel *Label1;
	TButton *buress;
	TButton *bufeedback;
	TButton *budelivery;
	TButton *bucontacts;
	TListView *lvPizzas;
	TGridPanelLayout *GridPanelLayout2;
	TImage *impizza;
	TLabel *impizzaname;
	TLabel *impizzanotes;
	TButton *buCheck;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TGridPanelLayout *GridPanelLayout4;
	TLabel *Label4;
	TLabel *impizzaprice;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TBindSourceDB *dbRestaurant;
	TLinkFillControlToField *LinkFillControlToField2;
	TLinkListControlToField *LinkListControlToField1;
	TListView *lvRestaurant;
	TLinkListControlToField *LinkListControlToField3;
	TGridPanelLayout *GridPanelLayout5;
	TImage *imrmap;
	TLabel *larname;
	TLabel *laradress;
	TGridPanelLayout *GridPanelLayout6;
	TLabel *Label6;
	TLabel *lartime;
	TLinkPropertyToField *LinkPropertyToFieldBitmap2;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	TLinkPropertyToField *LinkPropertyToFieldText6;
	TLinkPropertyToField *LinkPropertyToFieldText7;
	TStyleBook *StyleBook1;
	TToolBar *ToolBar1;
	TGridPanelLayout *GridPanelLayout7;
	TButton *buback;
	TLabel *latop;
	TButton *bumenu;
	TTabItem *tiorder;
	TMemo *Memo1;
	TMemo *Memo2;
	TBindSourceDB *dbFeedback;
	TGridPanelLayout *GridPanelLayout8;
	TLabel *Label2;
	TEdit *Edit1;
	TLabel *Label3;
	TLabel *Label7;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit5;
	TEdit *Edit7;
	TEdit *Edit9;
	TEdit *Edit11;
	TLabel *Label5;
	TLabel *Label8;
	TLabel *Label9;
	TLabel *Label10;
	TLabel *Label11;
	TEdit *Edit4;
	TButton *Button2;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TGrid *grFeedback;
	TButton *buotziv;
	TLinkGridToDataSource *LinkGridToDataSourcedbFeedback;
	TGridPanelLayout *GridPanelLayout9;
	TButton *Button1;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Label14;
	TEdit *Edit10;
	TLabel *Label15;
	TEdit *Edit12;
	TEdit *Edit6;
	TEdit *Edit8;
	void __fastcall bufeedbackClick(TObject *Sender);
	void __fastcall budeliveryClick(TObject *Sender);
	void __fastcall bucontactsClick(TObject *Sender);
	void __fastcall lvPizzasItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall lvRestaurantItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall bubackClick(TObject *Sender);
	void __fastcall bumenuClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buCheckClick(TObject *Sender);
	void __fastcall bupizzasClick(TObject *Sender);
	void __fastcall buressClick(TObject *Sender);
	void __fastcall buotzivClick(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
