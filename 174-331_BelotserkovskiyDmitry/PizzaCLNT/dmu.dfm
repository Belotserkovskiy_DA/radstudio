object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/')
    Connected = True
    Left = 192
    Top = 8
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmDatabase'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 192
    Top = 64
  end
  object cdsOrdering: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspOrdering'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsOrderingAfterPost
    Left = 112
    Top = 128
    object cdsOrderingID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsOrderingCREATE_DATETIME: TSQLTimeStampField
      FieldName = 'CREATE_DATETIME'
      Required = True
    end
    object cdsOrderingORDER_DATE: TDateField
      FieldName = 'ORDER_DATE'
    end
    object cdsOrderingORDER_TIME: TTimeField
      FieldName = 'ORDER_TIME'
    end
    object cdsOrderingPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object cdsOrderingPRICE: TSingleField
      FieldName = 'PRICE'
    end
    object cdsOrderingFIO: TWideStringField
      FieldName = 'FIO'
      Size = 280
    end
    object cdsOrderingPHONE: TWideStringField
      FieldName = 'PHONE'
      Size = 80
    end
    object cdsOrderingADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Size = 1020
    end
    object cdsOrderingNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 16000
    end
    object cdsOrderingSTATUS: TIntegerField
      FieldName = 'STATUS'
    end
  end
  object cdsProduct: TClientDataSet
    Active = True
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'dspProduct'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsProductAfterPost
    Left = 192
    Top = 128
    object cdsProductID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsProductNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 280
    end
    object cdsProductNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 16000
    end
    object cdsProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Required = True
      Size = 1
    end
    object cdsProductPRICE: TSingleField
      FieldName = 'PRICE'
      Required = True
    end
    object cdsProductSTICKER: TIntegerField
      FieldName = 'STICKER'
    end
  end
  object cdsRestaurant: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRestaurant'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsRestaurantAfterPost
    Left = 272
    Top = 128
    object cdsRestaurantID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsRestaurantNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 400
    end
    object cdsRestaurantADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Size = 1020
    end
    object cdsRestaurantHOURS: TWideStringField
      FieldName = 'HOURS'
      Size = 1020
    end
    object cdsRestaurantMAP: TBlobField
      FieldName = 'MAP'
      Size = 1
    end
  end
  object cdsFeedback: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFeedback'
    RemoteServer = DSProviderConnection1
    Left = 344
    Top = 128
    object cdsFeedbackID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsFeedbackNAME: TWideStringField
      FieldName = 'NAME'
      Size = 200
    end
    object cdsFeedbackMOBILE: TIntegerField
      FieldName = 'MOBILE'
    end
    object cdsFeedbackEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 200
    end
    object cdsFeedbackMESSAGE: TWideStringField
      FieldName = 'MESSAGE'
      Size = 1020
    end
  end
end
