//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
#include <IPPeerClient.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *cdsOrdering;
	TClientDataSet *cdsProduct;
	TClientDataSet *cdsRestaurant;
	TIntegerField *cdsOrderingID;
	TSQLTimeStampField *cdsOrderingCREATE_DATETIME;
	TDateField *cdsOrderingORDER_DATE;
	TTimeField *cdsOrderingORDER_TIME;
	TIntegerField *cdsOrderingPRODUCT_ID;
	TSingleField *cdsOrderingPRICE;
	TWideStringField *cdsOrderingFIO;
	TWideStringField *cdsOrderingPHONE;
	TWideStringField *cdsOrderingADDRESS;
	TWideStringField *cdsOrderingNOTES;
	TIntegerField *cdsOrderingSTATUS;
	TIntegerField *cdsProductID;
	TWideStringField *cdsProductNAME;
	TWideStringField *cdsProductNOTES;
	TBlobField *cdsProductIMAGE;
	TSingleField *cdsProductPRICE;
	TIntegerField *cdsProductSTICKER;
	TIntegerField *cdsRestaurantID;
	TWideStringField *cdsRestaurantNAME;
	TWideStringField *cdsRestaurantADDRESS;
	TWideStringField *cdsRestaurantHOURS;
	TBlobField *cdsRestaurantMAP;
	TClientDataSet *cdsFeedback;
	TIntegerField *cdsFeedbackID;
	TWideStringField *cdsFeedbackNAME;
	TIntegerField *cdsFeedbackMOBILE;
	TWideStringField *cdsFeedbackEMAIL;
	TWideStringField *cdsFeedbackMESSAGE;
	void __fastcall cdsOrderingAfterPost(TDataSet *DataSet);
	void __fastcall cdsProductAfterPost(TDataSet *DataSet);
	void __fastcall cdsRestaurantAfterPost(TDataSet *DataSet);
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TdmDatabaseClient* FdmDatabaseClient;
	TdmDatabaseClient* GetdmDatabaseClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmDatabaseClient* dmDatabaseClient = {read=GetdmDatabaseClient, write=FdmDatabaseClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
