//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::bufeedbackClick(TObject *Sender)
{
	tc->GotoVisibleTab(tifeed->Index);//��������� �� ������ �������
	buback->Enabled = false; //����������� ������ - "�����"
	bumenu->Enabled = true;  //����������� ������ - "����"
	latop->Text = "�������� �����"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::budeliveryClick(TObject *Sender)
{
	tc->GotoVisibleTab(tidelivery->Index);  //��������� �� ������ �������
	buback->Enabled = true; //����������� ������ - "�����"
	latop->Text = "������� ��������"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::bucontactsClick(TObject *Sender)
{
	tc->GotoVisibleTab(ticontact->Index); //��������� �� ������ �������
	buback->Enabled = true; //����������� ������ - "�����"
	latop->Text = "��������"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvPizzasItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(tipizza->Index); //��������� �� ������ �������
	buback->Enabled = false; //����������� ������ - "�����"
	bumenu->Enabled = true;  //����������� ������ - "����"
	latop->Text = "���� �����"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvRestaurantItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(tires->Index); //��������� �� ������ �������
	buback->Enabled = true; //����������� ������ - "�����"
	bumenu->Enabled = false;  //����������� ������ - "����"
	latop->Text = "��� ��������";  //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::bubackClick(TObject *Sender)
{
	if (tc->ActiveTab == tipizza) {
		tc->GotoVisibleTab(tipizzas->Index); //��������� �� ������ �������
		latop->Text = "������� ����"; //������������ ������� �����
	}
	else if (tc->ActiveTab == tires) {
		tc->GotoVisibleTab(tiress->Index); //��������� �� ������ �������
		buback->Enabled = false; //����������� ������ - "�����"
		bumenu->Enabled = true;  //����������� ������ - "����"
		latop->Text = "���� ���������"; //������������ ������� �����
	}
	else if (tc->ActiveTab == tipizzas) {
		tc->GotoVisibleTab(timenu->Index); //��������� �� ������ �������
		buback->Enabled = false; //����������� ������ - "�����"
		bumenu->Enabled = false;  //����������� ������ - "����"
		latop->Text = "������� �����"; //������������ ������� �����
	}
	else if (tc->ActiveTab == tiorder) {
		tc->GotoVisibleTab(tipizza->Index); //��������� �� ������ �������
		buback->Enabled = false; //����������� ������ - "�����"
		latop->Text = "���� �����"; //������������ ������� �����
	}
	else if (tc->ActiveTab == tireport) {
		tc->GotoVisibleTab(tifeed->Index); //��������� �� ������ �������
		buback->Enabled = false; //����������� ������ - "�����"
		latop->Text = "�������� �����"; //������������ ������� �����
	}
	else if (tc->ActiveTab == tidelivery) {
		tc->GotoVisibleTab(timenu->Index); //��������� �� ������ �������
		buback->Enabled = false; //����������� ������ - "�����"
		latop->Text = "������� �����"; //������������ ������� �����
	}
	else if (tc->ActiveTab == ticontact) {
		tc->GotoVisibleTab(timenu->Index); //��������� �� ������ �������
		buback->Enabled = false; //����������� ������ - "�����"
		latop->Text = "������� �����"; //������������ ������� �����
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::bumenuClick(TObject *Sender)
{
	tc->GotoVisibleTab(timenu->Index); //��������� �� ������ �������
	bumenu->Enabled = false; //����������� ������ - "����"
	latop->Text = "������� �����";  //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = timenu; //����� ��������� � ������ �������
	buback->Enabled = false; //����������� ������ - "�����"
	bumenu->Enabled = false; //����������� ������ - "����"
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCheckClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiorder->Index); //��������� �� ������ �������
	latop->Text = "�������� �����"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::bupizzasClick(TObject *Sender)
{
	tc->GotoVisibleTab(tipizzas->Index); //��������� �� ������ �������
	buback->Enabled = true; //����������� ������ - "�����"
	latop->Text = "������� ����"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buressClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiress->Index); //��������� �� ������ �������
	buback->Enabled = false; //����������� ������ - "�����"
	bumenu->Enabled = true;  //����������� ������ - "����"
	latop->Text = "���� ���������"; //������������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buotzivClick(TObject *Sender)
{
	tc->GotoVisibleTab(tireport->Index); //��������� �� ������ �������
	buback->Enabled = true; //����������� ������ - "�����"
	bumenu->Enabled = false; //����������� ������ - "����"
	latop->Text = "���������� ������"; //������������ ������� �����
}
//---------------------------------------------------------------------------

