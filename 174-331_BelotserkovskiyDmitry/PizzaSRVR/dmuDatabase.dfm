object dmDatabase: TdmDatabase
  OldCreateOrder = False
  Height = 386
  Width = 444
  object PizzaConnection: TSQLConnection
    ConnectionName = 'PizzaMag'
    DriverName = 'Firebird'
    Params.Strings = (
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False'
      
        'Database=C:\Users\VD\Documents\radstudio\174-331_Belotserkovskiy' +
        'Dmitry\PIZZADB.FDB')
    Connected = True
    Left = 192
    Top = 32
  end
  object taOrdering: TSQLDataSet
    CommandText = 'ORDERING'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzaConnection
    Left = 40
    Top = 128
  end
  object taProduct: TSQLDataSet
    CommandText = 'PRODUCT'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzaConnection
    Left = 192
    Top = 128
  end
  object dspOrdering: TDataSetProvider
    DataSet = taOrdering
    Left = 40
    Top = 200
  end
  object dspProduct: TDataSetProvider
    DataSet = taProduct
    Left = 192
    Top = 200
  end
  object dspRestaurant: TDataSetProvider
    DataSet = taRestaurant
    Left = 296
    Top = 200
  end
  object taRestaurant: TSQLDataSet
    CommandText = 'RESTAURANT'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzaConnection
    Left = 296
    Top = 128
  end
  object taFeedback: TSQLDataSet
    CommandText = 'FEEDBACK'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzaConnection
    Left = 384
    Top = 128
  end
  object dspFeedback: TDataSetProvider
    DataSet = taFeedback
    Left = 384
    Top = 200
  end
end
