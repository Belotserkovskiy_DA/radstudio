//---------------------------------------------------------------------------

#ifndef UInfoH
#define UInfoH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFInfo : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TLabel *Label1;
	TButton *Button1;
	TStyleBook *StyleBook1;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFInfo(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFInfo *FInfo;
//---------------------------------------------------------------------------
#endif
