//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "UMain.h"
#include <sstream>
#include "UInfo.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFMain *FMain;
//---------------------------------------------------------------------------
__fastcall TFMain::TFMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFMain::bstartClick(TObject *Sender)
{
	tc1->Next();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::banswerClick(TObject *Sender)
{
	mefinish->Lines->Add(
	L"������ �" + tc1->ActiveTab->Text +" - " +
	((((TControl *)Sender)->Tag == 1) ? L"�����" : L"�������")
	);
	((((TControl *)Sender)->Tag == 1) ? plus=plus+1 : minus=minus+1);
	std::string s;
	std::stringstream out;
	out << plus;
	s = out.str();
	if (plus + minus == 20)
	{
		if (plus > 16)
		{
		  mefinish->Lines->Add("���� ����!");
		}
		else
		{
		  mefinish->Lines->Add("���� �� ����!");
		}
	}
	tc1->Next();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::FormShow(TObject *Sender)
{
tc1->ActiveTab = ti1;
plus = 0;
minus = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFMain::tc1Change(TObject *Sender)
{
	pb1->Value = tc1->ActiveTab->Index;
}
//---------------------------------------------------------------------------


void __fastcall TFMain::binfoClick(TObject *Sender)
{
FInfo->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFMain::bexitClick(TObject *Sender)
{
tc1->ActiveTab = ti1;
plus = 0;
minus = 0;
mefinish->Lines->Clear();
}
//---------------------------------------------------------------------------

