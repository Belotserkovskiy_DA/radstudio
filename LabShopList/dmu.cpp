//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
UnicodeString FFileName;
const UnicodeString cNameDB = "labShoppingList.json";
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
	FFileName = System::Ioutils::TPath::GetDocumentsPath() + PathDelim + cNameDB;
	//
	if (FileExists(FFileName)) {
		taList->LoadFromFile(FFileName);
	} else {
		taList->Open();
	}
}
//---------------------------------------------------------------------------
void __fastcall Tdm::taListAfterInsert(TDataSet *DataSet)
{
    taListImageIndex->Value = -1;
}
//---------------------------------------------------------------------------
void __fastcall Tdm::taListAfterPost(TDataSet *DataSet)
{
	taList->SaveToFile(FFileName);
}
//---------------------------------------------------------------------------
void __fastcall Tdm::taListAfterDelete(TDataSet *DataSet)
{
      	taList->SaveToFile(FFileName);
}
//---------------------------------------------------------------------------
