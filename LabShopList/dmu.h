//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.StorageJSON.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDStanStorageJSONLink *FDStanStorageJSONLink1;
	TFDMemTable *taList;
	TImageList *il;
	TStringField *taListText;
	TStringField *taListDetail;
	TStringField *taListHeaderText;
	TIntegerField *taListImageIndex;
	TBooleanField *taListCheckmark;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall taListAfterInsert(TDataSet *DataSet);
	void __fastcall taListAfterPost(TDataSet *DataSet);
	void __fastcall taListAfterDelete(TDataSet *DataSet);
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
