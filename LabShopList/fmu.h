//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Objects3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbList;
	TSpeedButton *SpeedButton1;
	TSpeedButton *sbEdit;
	TButton *Button1;
	TButton *buAdd;
	TTabControl *tc;
	TTabItem *tiList;
	TTabItem *tiItem;
	TListView *lv;
	TLabel *Label1;
	TStyleBook *StyleBook1;
	TToolBar *ToolBar1;
	TLabel *Label2;
	TButton *buBack;
	TButton *buDel;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buSave;
	TButton *buCancel;
	TScrollBox *ScrollBox1;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *Layout1;
	TLabel *Label3;
	TLabel *Label4;
	TEdit *Edit1;
	TLabel *Label5;
	TComboBox *ComboBox1;
	TLabel *Label6;
	TMemo *Memo1;
	TGlyph *Glyph;
	TSwitch *Switch1;
	TLabel *Label7;
	TGridPanelLayout *GridPanelLayout3;
	TButton *buImagePrev;
	TButton *buImageClear;
	TButton *buImageNext;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkFillControlToField *LinkFillControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldImageIndex;
	TLinkControlToField *LinkControlToField3;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall lvUpdateObjects(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buCancelClick(TObject *Sender);
	void __fastcall buSaveClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageClearClick(TObject *Sender);
	void __fastcall GlyphChanged(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
