//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1MouseEnter(TObject *Sender)
{
Button1->Margins->Bottom = 0;
Button1->Margins->Top = 0;
Button1->Margins->Right = 0;
Button1->Margins->Left = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1MouseLeave(TObject *Sender)
{
	   Button1->Margins->Bottom = 20;
Button1->Margins->Top = 20;
Button1->Margins->Right = 50;
Button1->Margins->Left = 50;
}
//---------------------------------------------------------------------------
