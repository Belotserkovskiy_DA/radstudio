//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbO;
	TPanel *tbOptions;
	TButton *buNew;
	TButton *buClear;
	TButton *buPrevImage;
	TButton *buNextImage;
	TButton *buBringToFront;
	TButton *buBringToBack;
	TButton *Button8;
	TTrackBar *tbRotation;
	TButton *Button3;
	TLayout *ly;
	TButton *bus;
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buNewClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buPrevImageClick(TObject *Sender);
	void __fastcall buNextImageClick(TObject *Sender);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buBringToBackClick(TObject *Sender);
	void __fastcall tbRotationChange(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall busClick(TObject *Sender);
private:
	TSelection *FSel;
	void ReSetSelection (TObject *Sender);
		void AddSelection (TObject *Sender);
public:// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

