//---------------------------------------------------------------------------

#ifndef fmHelpH
#define fmHelpH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Thelp : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TLabel *label1;
	TButton *Button1;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Thelp(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Thelp *help;
//---------------------------------------------------------------------------
#endif
